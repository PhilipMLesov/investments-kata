<?php
namespace LendinvestKata\Classes;

use DateTime;
use LendinvestKata\Interfaces\TrancheInterface;

/**
 * Class Tranche
 * @package LendinvestKata\Classes
 */
class Tranche implements TrancheInterface
{
    /**
     * @var float
     */
    private $moneyInterestPercentage;

    /**
     * @var float
     */
    private $maximumAmount;

    /**
     * @var float
     */
    private $currentAmount;

    /**
     * @var
     */
    private $name;

    /**
     * @var array
     */
    private $investments;

    /**
     * Tranche constructor.
     * @param $name
     * @param $moneyInterestPercentage
     * @param $maximumAmount
     */
    public function __construct($name, $moneyInterestPercentage, $maximumAmount)
    {
        $this->moneyInterestPercentage = $moneyInterestPercentage;
        $this->maximumAmount = $maximumAmount;
        $this->name = $name;
        $this->currentAmount = 0;
        $this->investments = [];
    }

    /**
     * @return float
     */
    public function getMoneyInterestPercentage() : float
    {
        return $this->moneyInterestPercentage;
    }

    /**
     * @return float
     */
    public function getMaximumAmount() : float
    {
        return $this->maximumAmount;
    }

    /**
     * @return float
     */
    public function getCurrentAmount() : float
    {
        return $this->currentAmount;
    }

    /**
     * @param Investor $investor
     * @param float $amount
     * @param DateTime|null $date
     * @return array
     * @throws \InvalidArgumentException
     */
    public function createInvestment(Investor $investor, float $amount, DateTime $date = null) : array
    {
        if ($amount + $this->getCurrentAmount() > $this->getMaximumAmount()) {
            throw new \InvalidArgumentException('You\'ve exceeded the maximum amount possible for this tranche.');
        }

        $this->currentAmount += $amount;

        return $this->investments[] = [
            'investor' => $investor,
            'amount' => $amount,
            'date' => $date ? $date : new DateTime()
        ];
    }

    /**
     * @return mixed
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getAllInvestments() : array
    {
        return $this->investments;
    }
}