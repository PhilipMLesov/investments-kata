<?php
namespace LendinvestKata\Classes;

use LendinvestKata\Interfaces\VirtualWalletInterface;

/**
 * Class VirtualWallet
 * @package LendinvestKata\Classes
 */
class VirtualWallet implements VirtualWalletInterface
{
    /**
     * @var float
     */
    private $money = 0;

    /**
     * @return float
     */
    public function checkAmount() : float
    {
        return $this->money;
    }

    /**
     * @param float|int $money
     * @return float
     */
    public function storeMoney(float $money) : float
    {
        if ($money < 0) {
            throw new \InvalidArgumentException('You cannot store negative amount of money');
        }

        return $this->money += $money;
    }

    /**
     * @param $amount
     * @return float
     * @throws \InvalidArgumentException
     */
    public function getMoney($amount) : float
    {
        if ($amount > $this->money) {
            throw new \InvalidArgumentException('You\'ve exceeded the maximum amount of your virtual wallet possible.');
        }

        return $this->money -= $amount;
    }
}