<?php
namespace LendinvestKata\Classes;

use DateTime;
use LendinvestKata\Exceptions\NoTranchesAvailableException;
use LendinvestKata\Exceptions\TrancheDoesNotExistException;
use LendinvestKata\Interfaces\LoanInterface;

/**
 * Class Loan
 * @package LendinvestKata\Classes
 */
class Loan implements LoanInterface
{
    /**
     * @var DateTime
     */
    private $startDate;

    /**
     * @var DateTime
     */
    private $endDate;

    /**
     * @var
     */
    private $tranches;

    /**
     * Loan constructor.
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @throws \InvalidArgumentException
     */
    public function __construct(DateTime $startDate, DateTime $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;

        if ($this->startDate > $this->endDate) {
            throw new \InvalidArgumentException('Start date of loan in sooner than it\'s End date');
        }

        $this->tranches = [];
    }

    /**
     * @return mixed
     */
    public function getStartDate() : DateTime
    {
        return $this->startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate() : DateTime
    {
        return $this->endDate;
    }

    /**
     * @param $name
     * @param $moneyInterestPercentage
     * @param $maximumAmount
     * @return Tranche
     */
    public function createTranche($name, $moneyInterestPercentage, $maximumAmount) : Tranche
    {
        return $this->tranches[] = new Tranche($name, $moneyInterestPercentage, $maximumAmount);
    }

    /**
     * @param $name
     * @return mixed
     * @throws NoTranchesAvailableException
     * @throws TrancheDoesNotExistException
     */
    public function getTranche($name) : Tranche
    {
        if (empty($this->tranches)) {
            throw new NoTranchesAvailableException('No tranches created for this loan');
        }

        foreach ($this->tranches as $tranche) {
            if ($tranche->getName() == $name) {
                return $tranche;
            }
        }

        throw new TrancheDoesNotExistException("Tranche {$name} does not exist");
    }

    /**
     * @return array
     */
    public function getAllTranches()
    {
        return $this->tranches;
    }
}