<?php
namespace LendinvestKata\Classes;

use LendinvestKata\Interfaces\InvestorInterface;

/**
 * Class Investor
 * @package LendinvestKata\Classes
 */
class Investor implements InvestorInterface
{
    /**
     * @var VirtualWallet
     */
    private $virtualWallet;

    /**
     * @var
     */
    public $name;

    /**
     * Investor constructor.
     * @param $name
     * @param float|int $money
     */
    public function __construct($name, $money = 0)
    {
        $this->virtualWallet = new VirtualWallet();
        $this->virtualWallet->storeMoney($money);
        $this->name = $name;
    }

    /**
     * @return VirtualWallet
     */
    public function getVirtualWallet() : VirtualWallet
    {
        return $this->virtualWallet;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
}