<?php
namespace LendinvestKata\Interfaces;

use DateTime;
use LendinvestKata\Classes\Tranche;

/**
 * Interface LoanInterface
 * @package LendinvestKata\Interfaces
 */
interface LoanInterface
{
    /**
     * @return DateTime
     */
    public function getStartDate() : DateTime;

    /**
     * @return DateTime
     */
    public function getEndDate() : DateTime;

    /**
     * @param $name
     * @param $moneyInterestPercentage
     * @param $maximumAmount
     * @return Tranche
     */
    public function createTranche($name, $moneyInterestPercentage, $maximumAmount) : Tranche;

    /**
     * @param $name
     * @return Tranche
     */
    public function getTranche($name) : Tranche;
}