<?php
namespace LendinvestKata\Interfaces;

/**
 * Interface VirtualWalletInterface
 * @package LendinvestKata\Interfaces
 */
interface VirtualWalletInterface
{
    /**
     * @return float
     */
    public function checkAmount() : float;

    /**
     * @param $money
     * @return float
     */
    public function storeMoney(float $money) : float;

    /**
     * @param $amount
     * @return float
     */
    public function getMoney($amount) : float;
}