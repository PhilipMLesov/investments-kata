<?php
namespace LendinvestKata\Interfaces;

use DateTime;
use LendinvestKata\Classes\Investor;

/**
 * Interface TrancheInterface
 * @package LendinvestKata\Interfaces
 */
interface TrancheInterface
{
    /**
     * @return float
     */
    public function getMoneyInterestPercentage() : float;

    /**
     * @return float
     */
    public function getMaximumAmount() : float;

    /**
     * @return float
     */
    public function getCurrentAmount() : float;

    /**
     * @param Investor $investor
     * @param float $amount
     * @param DateTime|null $date
     * @return array
     */
    public function createInvestment(Investor $investor, float $amount, DateTime $date = null) : array;

    /**
     * @return string
     */
    public function getName() : string;

    /**
     * @return array
     */
    public function getAllInvestments() : array;
}