<?php
namespace LendinvestKata\Interfaces;

use LendinvestKata\Classes\VirtualWallet;

/**
 * Interface InvestorInterface
 * @package LendinvestKata\Interfaces
 */
interface InvestorInterface
{
    /**
     * @return VirtualWallet
     */
    public function getVirtualWallet() : VirtualWallet;

    /**
     * @return string
     */
    public function getName() : string;
}