<?php
namespace LendinvestKata\Exceptions;

use Exception;

/**
 * Class TrancheDoesNotExistException
 * @package LendinvestKata\Exceptions
 */
class TrancheDoesNotExistException extends Exception
{
    //
}