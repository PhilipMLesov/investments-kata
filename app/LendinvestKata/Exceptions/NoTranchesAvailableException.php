<?php
namespace LendinvestKata\Exceptions;

use Exception;

/**
 * Class NoTranchesAvailableException
 * @package LendinvestKata\Exceptions
 */
class NoTranchesAvailableException extends Exception
{
    //
}