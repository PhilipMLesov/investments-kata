<?php
namespace LendinvestKata\Exceptions;

use Exception;

/**
 * Class NoOpenLoansProvidedException
 * @package LendinvestKata\Exceptions
 */
class NoOpenLoansProvidedException extends Exception
{
    //
}