<?php
namespace LendinvestKata\Exceptions;

use Exception;

/**
 * Class LoanNotOpenException
 * @package LendinvestKata\Exceptions
 */
class LoanNotOpenException extends Exception
{
    //
}