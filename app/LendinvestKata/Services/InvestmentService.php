<?php
namespace LendinvestKata\Services;

use DateTime;
use LendinvestKata\Classes\Investor;
use LendinvestKata\Classes\Loan;
use LendinvestKata\Exceptions\LoanNotOpenException;

/**
 * Class InvestmentService
 * @package LendinvestKata\Services
 */
class InvestmentService
{
    /**
     * @param Loan $loan
     * @param string $trancheName
     * @param Investor $investor
     * @param float $amount
     * @param DateTime $date
     * @return string
     */
    public function invest(
        Loan $loan,
        string $trancheName,
        Investor $investor,
        float $amount,
        DateTime $date = null
    ) : string {
        $this->assertLoanIsOpen($loan->getStartDate(), $loan->getEndDate(), $date);

        $tranche = $loan->getTranche($trancheName);
        $investor->getVirtualWallet()->getMoney($amount);
        $tranche->createInvestment($investor, $amount, $date);

        return 'ok';
    }

    /**
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param DateTime $currentDate
     * @throws LoanNotOpenException
     */
    public function assertLoanIsOpen(DateTime $startDate, DateTime $endDate, DateTime $currentDate = null)
    {
        $currentDate = $currentDate ? $currentDate : new DateTime();

        if ($currentDate > $endDate || $currentDate < $startDate) {
            throw new LoanNotOpenException('Loan not open. Investment not made.');
        }
    }
}