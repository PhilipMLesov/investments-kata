<?php
namespace LendinvestKata\Services;

/**
 * Class PrintSuccessfulPaymentService
 * @package LendinvestKata\Services
 */
class PrintSuccessfulPaymentService
{
    /**
     * @param $investors
     * @internal param $investor
     */
    public static function print($investors) {
        foreach ($investors as $investor) {
            echo "<b>\"" . $investor['investor']->getName() . "\"</b> earns <b>" . $investor['earnedMoney'] . "</b> pounds <br />";
        }
    }
}