<?php
namespace LendinvestKata\Services;

use DateTime;
use LendinvestKata\Exceptions\NoOpenLoansProvidedException;

/**
 * Class ProcessInterestService
 * @package LendinvestKata\Services
 */
class ProcessInterestService
{
    /**
     * @var array
     */
    private $recordedInvestors = [];

    /**
     * Processes interests for given dates and loans. Returns investors with processed interest and invested money.
     *
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param array $loans
     * @return array
     * @throws NoOpenLoansProvidedException
     * @internal param array $dates
     */
    public function processTimePeriod(DateTime $startDate, DateTime $endDate, array $loans)
    {
        $atLeastOneOpenLoan = false;

        $daysInPeriodDiff = $startDate->diff($endDate)->days + 1;

        foreach($loans as $loan) {
            if ($endDate < $loan->getStartDate() || $startDate > $loan->getEndDate()) {
                continue;
            }

            $atLeastOneOpenLoan = true;

            foreach($loan->getAllTranches() as $tranche) {
                foreach($tranche->getAllInvestments() as $investment) {
                    $this->recordInvestor($investment['investor']);

                    $investmentPeriod = $endDate->diff($investment['date'])->days + 1;

                    $investmentPeriodToTotalLoanPeriodPercentage = $investmentPeriod / $daysInPeriodDiff;
                    $percentageInterestForInvestment = ($investmentPeriodToTotalLoanPeriodPercentage * $tranche->getMoneyInterestPercentage()) / 100;

                    $moneyFromInvestment = (float)number_format($percentageInterestForInvestment * $tranche->getCurrentAmount(), 2, '.', '');

                    foreach ($this->recordedInvestors as &$currentInvestor) {
                        if ($investment['investor']->getName() == $currentInvestor['investor']->getName()) {
                            $currentInvestor['investedMoney'] += $investment['amount'];
                            $currentInvestor['earnedMoney'] += $moneyFromInvestment;
                        }
                    }
                }
            }
        }

        if (!$atLeastOneOpenLoan) {
            throw new NoOpenLoansProvidedException('There were no open loans provided for that period.');
        }

        return $this->recordedInvestors;
    }

    /**
     * @param $investor
     * @return array|mixed
     */
    private function recordInvestor($investor)
    {
        foreach ($this->recordedInvestors as $currentInvestor) {
            if ($currentInvestor['investor']->getName() == $investor->getName()) {
                return $currentInvestor;
            }
        }

        return $this->recordedInvestors[] = ['investor' => $investor, 'investedMoney' => 0, 'earnedMoney' => 0];
    }
}