<?php

// Get initialization
require_once 'init.php';

use LendinvestKata\Classes\Investor;
use LendinvestKata\Classes\Loan;
use LendinvestKata\Services\InvestmentService;
use LendinvestKata\Services\PrintSuccessfulPaymentService;
use LendinvestKata\Services\ProcessInterestService;

$loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));
$loan->createTranche('A', 3, 1000);
$loan->createTranche('B', 6, 1000);

$investmentService = new InvestmentService();

$investor1 = new Investor('Investor 1');
$investor1->getVirtualWallet()->storeMoney(1000);
echo $investmentService->invest($loan, 'A', $investor1, 1000, new DateTime('03-10-2015')), "<br />";

$investor2 = new Investor('Investor 2');
$investor2->getVirtualWallet()->storeMoney(1);
try {
    $investmentService->invest($loan, 'A', $investor2, 1, new DateTime('04-10-2015'));
} catch(Exception $exception) {
    echo $exception->getMessage() . "<br />";
}

$investor3 = new Investor('Investor 3');
$investor3->getVirtualWallet()->storeMoney(500);
echo $investmentService->invest($loan, 'B', $investor3, 500, new DateTime('10-10-2015')), "<br />";

$investor4= new Investor('Investor 4');
$investor4->getVirtualWallet()->storeMoney(1100);
try {
    $investmentService->invest($loan, 'B', $investor4, 1100, new DateTime('25-10-2015'));
} catch(Exception $exception) {
    echo $exception->getMessage() . "<br />";
}

$processInterestService = new ProcessInterestService();
$processedInvestors = $processInterestService->processTimePeriod(new DateTime('01-10-2015'), new DateTime('31-10-2015'), [$loan]);
PrintSuccessfulPaymentService::print($processedInvestors);