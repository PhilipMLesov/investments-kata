<?php

use LendinvestKata\Classes\Tranche;
use LendinvestKata\Interfaces\LoanInterface;
use PHPUnit\Framework\TestCase;
use LendinvestKata\Classes\Loan;

/**
 * Class LoanTest
 */
class LoanTest extends TestCase
{
    public function testLoanImplementsLoanInterface()
    {
        $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));
        $this->assertTrue($loan instanceof LoanInterface);
    }

    public function testLoanHasStartDate()
    {
        $expected = new DateTime('01-10-2015');
        $loan = new Loan($expected, new DateTime('15-11-2015'));

        $this->assertEquals($expected, $loan->getStartDate());
    }

    public function testLoanHasEndDate()
    {
        $expected = new DateTime('15-11-2015');
        $loan = new Loan(new DateTime('01-10-2015'), $expected);

        $this->assertEquals($expected, $loan->getEndDate());
    }

    public function testGetStartDateReturnsDateTime()
    {
        $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));

        $this->assertTrue($loan->getStartDate() instanceof DateTime);
    }

    public function testGetEndDateReturnsDateTime()
    {
        $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));

        $this->assertTrue($loan->getEndDate() instanceof DateTime);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testStartDateNotGreaterThanEndDate()
    {
        $startDate = new DateTime('15-11-2015');
        $endDate = new DateTime('01-10-2015');

        $loan = new Loan($startDate, $endDate);
    }

    public function testCanCreateTrancheInLoan()
    {
        $startDate = new DateTime('01-10-2015');
        $endDate = new DateTime('15-11-2015');

        $loan = new Loan($startDate, $endDate);
        $loan->createTranche('A', 3, 1000);

        $this->assertInstanceOf(Tranche::class, $loan->getTranche('A'));
    }

    /**
     * @expectedException LendinvestKata\Exceptions\NoTranchesAvailableException
     */
    public function testGetTrancheOnEmptyTranchesReturnsNoTranchesAvailableException()
    {
        $startDate = new DateTime('01-10-2015');
        $endDate = new DateTime('15-11-2015');

        $loan = new Loan($startDate, $endDate);
        $loan->getTranche('A');
    }

    /**
     * @expectedException LendinvestKata\Exceptions\TrancheDoesNotExistException
     */
    public function testGetNotExistingTranchReturnsTrancheDoesNotExistException()
    {
        $startDate = new DateTime('01-10-2015');
        $endDate = new DateTime('15-11-2015');

        $loan = new Loan($startDate, $endDate);
        $loan->createTranche('A', 3, 1000);
        $loan->getTranche('B');
    }

    public function testCreateTrancheReturnsTranche()
    {
        $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));

        $this->assertInstanceOf(Tranche::class, $loan->createTranche('A', 3, 1000));
    }

    public function testGetTrancheReturnsTranche()
    {
        $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));
        $loan->createTranche('A', 3, 1000);

        $this->assertInstanceOf(Tranche::class, $loan->getTranche('A'));
    }

    public function testGetAllTranchesIncludesCreatedTranche()
    {
        $expected = new Tranche('A', 3, 1000);
        $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));
        $loan->createTranche('A', 3, 1000);

        $this->assertEquals([$expected], $loan->getAllTranches());
    }

    public function testGetAllTranchesReturnTheTwoTranchesAdded()
    {
        $expected[] = new Tranche('A', 3, 1000);
        $expected[] = new Tranche('B', 6, 1000);

        $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));
        $loan->createTranche('A', 3, 1000);
        $loan->createTranche('B', 6, 1000);

        $this->assertEquals($expected, $loan->getAllTranches());
    }
}