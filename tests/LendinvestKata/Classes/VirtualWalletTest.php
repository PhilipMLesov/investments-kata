<?php

use LendinvestKata\Classes\VirtualWallet;
use LendinvestKata\Interfaces\VirtualWalletInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class VirtualWalletTest
 */
class VirtualWalletTest extends TestCase
{
    public function testLoanImplementsLoanInterface()
    {
        $virtualWallet = new VirtualWallet();
        
        $this->assertTrue($virtualWallet instanceof VirtualWalletInterface);
    }

    public function testCheckAmountReturnsZero()
    {
        $virtualWallet = new VirtualWallet();

        $this->assertEquals(0, $virtualWallet->checkAmount());
    }

    public function testCheckAmountReturnsInteger()
    {
        $virtualWallet = new VirtualWallet();
        $virtualWallet->storeMoney(30);

        $this->assertInternalType('float', $virtualWallet->checkAmount());
    }

    public function testStoreMoneyReturnsInteger()
    {
        $virtualWallet = new VirtualWallet();

        $this->assertInternalType('float', $virtualWallet->storeMoney(30));
    }

    public function testGetMoneyReturnsInteger()
    {
        $virtualWallet = new VirtualWallet();
        $virtualWallet->storeMoney(30);

        $this->assertInternalType('float', $virtualWallet->getMoney(30));
    }

    public function testStoreMoney()
    {
        $virtualWallet = new VirtualWallet();
        $expected = 30;

        $this->assertEquals($expected, $virtualWallet->storeMoney($expected));
    }

    public function testStoreMoneyTwoTimes()
    {
        $virtualWallet = new VirtualWallet();
        $firstAmount = 30;
        $secondAmount = 60;
        $expected = $firstAmount + $secondAmount;

        $virtualWallet->storeMoney($firstAmount);
        $virtualWallet->storeMoney($secondAmount);

        $this->assertEquals($expected, $virtualWallet->checkAmount());
    }

    public function testGetMoney()
    {
        $virtualWallet = new VirtualWallet();
        $money = 30;
        $expected = 0;

        $virtualWallet->storeMoney($money);
        $this->assertEquals($expected, $virtualWallet->getMoney($money));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetMoreMoneyThanPossibleReturnsInvalidArgumentException()
    {
        $virtualWallet = new VirtualWallet();
        $money = 30;

        $virtualWallet->storeMoney($money);
        $virtualWallet->getMoney(90);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testStoreLessThanZeroMoneyReturnsInvalidArgumentException()
    {
        $virtualWallet = new VirtualWallet();
        $money = -30;

        $virtualWallet->storeMoney($money);
    }
}