<?php

use LendinvestKata\Classes\Investor;
use LendinvestKata\Interfaces\TrancheInterface;
use PHPUnit\Framework\TestCase;
use LendinvestKata\Classes\Tranche;

/**
 * Class TrancheTest
 */
class TrancheTest extends TestCase
{
    public function testTrancheImplementsTrancheInterface()
    {
        $tranche = new Tranche('A', 3, 1000);

        $this->assertTrue($tranche instanceof TrancheInterface);
    }

    public function testTrancheHasMoneyInterestPercentage()
    {
        $expected = 3;
        $tranche = new Tranche('A', $expected, 1000);

        $this->assertEquals($expected, $tranche->getMoneyInterestPercentage());
    }

    public function testGetMoneyInterestPercentageReturnsInt()
    {
        $tranche = new Tranche('A', 3, 1000);

        $this->assertInternalType('float', $tranche->getMoneyInterestPercentage());
    }

    public function testTrancheHasMaximumAmount()
    {
        $expected = 1000;
        $tranche = new Tranche('A', 3, $expected);

        $this->assertEquals($expected, $tranche->getMaximumAmount());
    }

    public function testGetCurrentAmountReturnsInt()
    {
        $tranche = new Tranche('A', 3, 1000);

        $this->assertInternalType('float', $tranche->getCurrentAmount());
    }

    public function testTrancheHasZeroCurrentAmount()
    {
        $expected = 0;
        $tranche = new Tranche('A', 3, 1000);

        $this->assertEquals($expected, $tranche->getCurrentAmount());
    }

    public function testTrancheGetMaximumAmountReturnsInt()
    {
        $tranche = new Tranche('A', 3, 1000);

        $this->assertInternalType('float', $tranche->getMaximumAmount());
    }

    public function testInvestOnTranche()
    {
        $expected = 30;
        $tranche = new Tranche('A', 3, 1000);

        $investor = $this->getMockBuilder(Investor::class)
                        ->setConstructorArgs(['Investor 1', 1000])
                        ->getMock();

        $tranche->createInvestment($investor, 30);
        $this->assertEquals($expected, $tranche->getCurrentAmount());
    }

    public function testInvestReturnsInvestment()
    {
        $date = new DateTime();
        $tranche = new Tranche('A', 3, 1000);

        $investor = $this->getMockBuilder(Investor::class)
            ->setConstructorArgs(['Investor 1', 1000])
            ->getMock();

        $expected = ['investor' => $investor, 'amount' => 300, 'date' => $date];

        $investment = $tranche->createInvestment($investor, 300, $date);

        $this->assertEquals($expected, $investment);
    }

    public function testInvestMoreThanOnceOnTranche()
    {
        $expected = 120;
        $tranche = new Tranche('A', 3, 1000);

        $investor = $this->getMockBuilder(Investor::class)
            ->setConstructorArgs(['Investor 1', 1000])
            ->getMock();

        $tranche->createInvestment($investor, 30);
        $tranche->createInvestment($investor, 90);

        $this->assertEquals($expected, $tranche->getCurrentAmount());
    }

    public function testTrancheName()
    {
        $expected = 'A';
        $tranche = new Tranche($expected, 3, 1000);

        $this->assertEquals($expected, $tranche->getName());
    }

    public function testGetNameReturnsString()
    {
        $tranche = new Tranche('A', 3, 1000);

        $this->assertInternalType('string', $tranche->getName());
    }

    public function testTrancheHasEmptyInvestments()
    {
        $tranche = new Tranche('A', 3, 1000);

        $this->assertEquals([], $tranche->getAllInvestments());
    }

    public function testInvestCreatesInvestment()
    {
        $tranche = new Tranche('A', 3, 1000);
        $date = new DateTime();

        $investor = $this->getMockBuilder(Investor::class)
            ->setConstructorArgs(['Investor 1', 1000])
            ->getMock();

        $tranche->createInvestment($investor, 1000, $date);
        $investments = $tranche->getAllInvestments();

        $this->assertEquals(['investor' => $investor, 'amount' => 1000, 'date' => $date], $investments[0]);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testInvestOnTrancheWithNoAvailableAmountToInvestReturnsExceededMaximumAmountException()
    {
        $tranche = new Tranche('A', 3, 1000);

        $investor = $this->getMockBuilder(Investor::class)
            ->setConstructorArgs(['Investor 1', 1000])
            ->getMock();

        $tranche->createInvestment($investor, 1001);
    }
}