<?php

use LendinvestKata\Classes\Investor;
use LendinvestKata\Classes\VirtualWallet;
use LendinvestKata\Interfaces\InvestorInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class InvestorTest
 */
class InvestorTest extends TestCase
{
    public function testInvestorImplementsInvestorInterface()
    {
        $investor = new Investor('Investor 1');
        $this->assertTrue($investor instanceof InvestorInterface);
    }

    public function testInvestorHasName()
    {
        $expected = 'Investor 1';
        $investor = new Investor($expected);

        $this->assertEquals($expected, $investor->getName());
    }

    public function testInvestorGetNameReturnsString()
    {
        $investor = new Investor('Investor 1');

        $this->assertInternalType('string', $investor->getName());
    }

    public function testInvestorHasWallet()
    {
        $investor = new Investor('Investor 1');

        $this->assertInstanceOf(VirtualWallet::class, $investor->getVirtualWallet());
    }

    public function testInvestorHasEmptyVirtualWallet()
    {
        $investor = new Investor('Investor 1');

        $this->assertEquals(0, $investor->getVirtualWallet()->checkAmount());
    }

    public function testInvestorHasMoneyInVirtualWallet()
    {
        $expected = 30;
        $investor = new Investor('Investor 1', $expected);

        $this->assertEquals($expected, $investor->getVirtualWallet()->checkAmount());
    }

    public function testInvestorGetsMoneyInVirtualWallet()
    {
        $expected = 30;
        $investor = new Investor('Investor 1');

        $this->assertEquals($expected, $investor->getVirtualWallet()->storeMoney($expected));
    }
}