<?php

use LendinvestKata\Classes\Investor;
use LendinvestKata\Classes\Loan;
use LendinvestKata\Classes\Tranche;
use LendinvestKata\Services\InvestmentService;
use PHPUnit\Framework\TestCase;

/**
 * Class InvestmentServiceTest
 */
class InvestmentServiceTest extends TestCase
{
    public function testInvestActuallyInvestsOnTranche()
    {
        $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));
        $loan->createTranche('A', 3, 1000);

        $investor = new Investor('Investor 1');
        $investor->getVirtualWallet()->storeMoney(1000);

        $investmentService = new InvestmentService();
        $this->assertEquals('ok', $investmentService->invest($loan, 'A', $investor, 1000, new DateTime('15-11-2015')));
    }

    /**
     * @expectedException LendinvestKata\Exceptions\LoanNotOpenException
     */
    public function testInvestOnClosedLoanReturnsLoanNotOpenException()
    {
        $tranche = new Tranche('A', 3, 1000);

        $investor = $this->getMockBuilder(Investor::class)
            ->setConstructorArgs(['Investor 1', 1000])
            ->getMock();

        $loan = $this->getMockBuilder(Loan::class)
            ->setConstructorArgs([new DateTime('01-10-2015'), new DateTime('15-11-2015')])
            ->setMethods(['createTranche', 'getTranche'])
            ->getMock();

        $loan->expects($this->once())
            ->method('createTranche')
            ->will($this->returnValue($tranche));

        $loan->createTranche('A', 3, 1000);

        $investmentService = new InvestmentService();
        $investmentService->invest($loan, 'A', $investor, 1000, new DateTime('16-11-2015'));
    }

    /**
     * @expectedException LendinvestKata\Exceptions\LoanNotOpenException
     */
    public function testInvestOnStillNotOpenedLoanReturnsLoanNotOpenException()
    {
        $tranche = new Tranche("A", 3, 1000);

        $investor = $this->getMockBuilder(Investor::class)
            ->setConstructorArgs(['Investor 1', 1000])
            ->getMock();

        $loan = $this->getMockBuilder(Loan::class)
            ->setConstructorArgs([new DateTime('01-10-2015'), new DateTime('15-11-2015')])
            ->setMethods(['createTranche', 'getTranche'])
            ->getMock();

        $loan->expects($this->once())
            ->method('createTranche')
            ->will($this->returnValue($tranche));

        $loan->createTranche('A', 3, 1000);

        $investmentService = new InvestmentService();
        $investmentService->invest($loan, 'A', $investor, 1000, new DateTime('30-09-2015'));
    }

    /**
     * @expectedException LendinvestKata\Exceptions\LoanNotOpenException
     */
    public function testAssertLoanIsStillNotOpenedReturnsLoanNotOpenException()
    {
        $investor = $this->getMockBuilder(Investor::class)
            ->setConstructorArgs(['Investor 1', 1000])
            ->getMock();

        $loan = $this->getMockBuilder(Loan::class)
            ->setConstructorArgs([new DateTime('01-10-2015'), new DateTime('15-11-2015')])
            ->setMethods(['createTranche', 'getTranche'])
            ->getMock();

        $investmentService = new InvestmentService();
        $investmentService->invest($loan, 'A', $investor, 1000, new DateTime('01-09-2015'));
    }

    /**
     * @expectedException LendinvestKata\Exceptions\LoanNotOpenException
     */
    public function testAssertLoanIsClosedReturnsLoanNotOpenException()
    {
        $investor = $this->getMockBuilder(Investor::class)
            ->setConstructorArgs(['Investor 1', 1000])
            ->getMock();

        $loan = $this->getMockBuilder(Loan::class)
            ->setConstructorArgs([new DateTime('01-10-2015'), new DateTime('15-11-2015')])
            ->setMethods(['createTranche', 'getTranche'])
            ->getMock();

        $investmentService = new InvestmentService();
        $investmentService->invest($loan, 'A', $investor, 1000, new DateTime('16-11-2015'));
    }
}