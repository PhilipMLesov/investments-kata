<?php

use LendinvestKata\Classes\Investor;
use LendinvestKata\Classes\Loan;
use LendinvestKata\Services\InvestmentService;
use LendinvestKata\Services\ProcessInterestService;
use PHPUnit\Framework\TestCase;

/**
 * Class ProcessInterestServiceTest
 */
class ProcessInterestServiceTest extends TestCase
{
    /**
     * @expectedException LendinvestKata\Exceptions\NoOpenLoansProvidedException
     */
    public function testNoOpenLoansReturnNoOpenLoansProvidedException()
    {
        $loans[] = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));
        $loans[] = new Loan(new DateTime('01-11-2015'), new DateTime('30-11-2015'));

        $processInterestService = new ProcessInterestService();
        $processInterestService->processTimePeriod(new DateTime('01-09-2015'), new DateTime('30-09-2015'), $loans);
    }

    public function testInvestor1CorrectInterestToMoneyCalculation()
    {
        $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));
        $loan->createTranche('A', 3, 1000);

        $investor = new Investor('Investor 1');
        $investor->getVirtualWallet()->storeMoney(1000);

        $expected = [[
            'investor' => $investor,
            'investedMoney' => 1000,
            'earnedMoney' => 28.06
        ]];

        $investmentService = new InvestmentService();
        $investmentService->invest($loan, 'A', $investor, 1000, new DateTime('03-10-2015'));

        $loans[] = $loan;
        $processInterestService = new ProcessInterestService();
        $processedInvestors = $processInterestService->processTimePeriod(new DateTime('01-10-2015'), new DateTime('31-10-2015'), $loans);

        $this->assertEquals($expected, $processedInvestors);
    }

    public function testInvestor2CorrectInterestToMoneyCalculation()
    {
        $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));
        $loan->createTranche('B', 6, 1000);

        $investor = new Investor('Investor 2');
        $investor->getVirtualWallet()->storeMoney(500);

        $expected = [[
            'investor' => $investor,
            'investedMoney' => 500,
            'earnedMoney' => 21.29
        ]];

        $investmentService = new InvestmentService();
        $investmentService->invest($loan, 'B', $investor, 500, new DateTime('10-10-2015'));

        $loans[] = $loan;
        $processInterestService = new ProcessInterestService();
        $processedInvestors = $processInterestService->processTimePeriod(new DateTime('01-10-2015'), new DateTime('31-10-2015'), $loans);

        $this->assertEquals($expected, $processedInvestors);
    }

    public function testTwoInvestorsOnDifferentTranchesGetCorrectInterestToMoneyCalculation()
    {
        $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));
        $loan->createTranche('A', 3, 1000);
        $loan->createTranche('B', 6, 1000);

        $investor1 = new Investor('Investor 1');
        $investor1->getVirtualWallet()->storeMoney(1000);
        $investor2 = new Investor('Investor 2');
        $investor2->getVirtualWallet()->storeMoney(500);

        $expected = [[
            'investor' => $investor1,
            'investedMoney' => 1000,
            'earnedMoney' => 28.06
        ], [
            'investor' => $investor2,
            'investedMoney' => 500,
            'earnedMoney' => 21.29
        ]];

        $investmentService = new InvestmentService();
        $investmentService->invest($loan, 'A', $investor1, 1000, new DateTime('03-10-2015'));
        $investmentService->invest($loan, 'B', $investor2, 500, new DateTime('10-10-2015'));

        $loans[] = $loan;
        $processInterestService = new ProcessInterestService();
        $processedInvestors = $processInterestService->processTimePeriod(new DateTime('01-10-2015'), new DateTime('31-10-2015'), $loans);

        $this->assertEquals($expected, $processedInvestors);
    }

    public function testSingleInvestorOnDifferentLoansGetCorrectInterestToMoneyCalculation()
    {
        $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));
        $loan->createTranche('A', 3, 1000);

        $loan2 = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));
        $loan2->createTranche('A', 3, 1000);

        $investor1 = new Investor('Investor 1');
        $investor1->getVirtualWallet()->storeMoney(2000);

        $expected = [[
            'investor' => $investor1,
            'investedMoney' => 2000,
            'earnedMoney' => 56.12
        ]];

        $investmentService = new InvestmentService();
        $investmentService->invest($loan, 'A', $investor1, 1000, new DateTime('03-10-2015'));
        $investmentService->invest($loan2, 'A', $investor1, 1000, new DateTime('03-10-2015'));

        $loans = [$loan, $loan2];

        $processInterestService = new ProcessInterestService();
        $processedInvestors = $processInterestService->processTimePeriod(new DateTime('01-10-2015'), new DateTime('31-10-2015'), $loans);

        $this->assertEquals($expected, $processedInvestors);
    }

    public function testRecordInvestorActuallyRecordsAnInvestor()
    {
        $investor = $this->getMockBuilder(Investor::class)
            ->setConstructorArgs(['Investor 1', 1000])
            ->getMock();

        $expected = ['investor' => $investor, 'investedMoney' => 0, 'earnedMoney' => 0];

        $recordInvestor = self::getMethod('recordInvestor');
        $processInterestService = new ProcessInterestService();

        $this->assertEquals($expected, $recordInvestor->invokeArgs($processInterestService, array($investor)));
    }

    public function testRecordInvestorReturnsInvestorWhenAlreadyRecorded()
    {
        $investor = $this->getMockBuilder(Investor::class)
            ->setConstructorArgs(['Investor 1', 1000])
            ->getMock();

        $expected = ['investor' => $investor, 'investedMoney' => 0, 'earnedMoney' => 0];

        $recordInvestor = self::getMethod('recordInvestor');
        $processInterestService = new ProcessInterestService();
        $recordInvestor->invokeArgs($processInterestService, array($investor));

        $this->assertEquals($expected, $recordInvestor->invokeArgs($processInterestService, array($investor)));
    }

    /**
     * @param $name
     * @return mixed
     */
    protected static function getMethod($name) {
        $class = new ReflectionClass(ProcessInterestService::class);

        $method = $class->getMethod($name);
        $method->setAccessible(true);

        return $method;
    }
}