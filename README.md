Lendinvest Kata Project

Requirements
============

* PHP >= 7.1
* PHPUnit >= 6.5

Installation
============

    composer update

Install the needed packages

Usage
=====

Create a loan with start and end date

    $loan = new Loan(new DateTime('01-10-2015'), new DateTime('15-11-2015'));

Create tranche for already created loan. Set it's name, money interest percentage and maximum amount available

    $loan->createTranche('A', 3, 1000);

Create investor with it's name required. Can send second param - money, to be stored in his Virtual Wallet

    $investor1 = new Investor('Investor 1');

Store money to already created investor

    $investor1->getVirtualWallet()->storeMoney(1000);

Static method to create an investment given a loan, it's tranche's name, investor, money and optionally date(if not send
current date is used as date of investment)

    $investmentService = new InvestmentService();
    $investmentService->invest($loan, 'A', $investor1, 1000, new DateTime('03-10-2015'));

Using service to process earnings for all investors participating in given loans. processTimePeriod expects start and end date of calculations
and array of loans to loop

    $processInterestService = new ProcessInterestService();
    $processedInvestors = $processInterestService->processTimePeriod(new DateTime('01-10-2015'), new DateTime('31-10-2015'), [$loan]);

Using PrintSuccessfulPaymentService to print the earnings to the screen

    PrintSuccessfulPaymentService::print($processedInvestors);

Credits
=======

Philip Lesov