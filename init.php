<?php
/**
 * Created by PhpStorm.
 * User: philiplesov
 * Date: 12/6/17
 * Time: 6:43 PM
 */


// Activate error reporting
ini_set('display_errors', 1);

define( 'ROOT_DIR', dirname(__FILE__) );

// Require autoload
require_once __DIR__ . '/vendor/autoload.php';

date_default_timezone_set('Europe/London');

set_exception_handler(function (Throwable $exception) {
    echo $exception->getMessage();
});